package chainmaker_sdk_go

import (
	"fmt"
	"time"

	"chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
)

const (
	// DefaultRetryLimit 默认轮训交易结果最大次数
	DefaultRetryLimit = 10
	// DefaultRetryInterval 默认每次轮训交易结果时的等待时间，单位ms
	DefaultRetryInterval = 500
)

func (cc *ChainClient) getSyncResult(txId string) (*common.ContractResult, error) {
	var (
		txInfo *common.TransactionInfo
		err    error
	)

	err = retry.Retry(func(uint) error {
		txInfo, err = cc.GetTxByTxId(txId)
		if err != nil {
			return err
		}

		return nil
	},
		strategy.Wait(time.Duration(cc.retryInterval)*time.Millisecond),
		strategy.Limit(uint(cc.retryLimit)),
	)

	if err != nil {
		return nil, fmt.Errorf("get tx by txId [%s] failed, %s", txId, err.Error())
	}
	if txInfo == nil || txInfo.Transaction == nil || txInfo.Transaction.Result == nil {
		return nil, fmt.Errorf("get result by txId [%s] failed, %+v", txId, txInfo)
	}
	return txInfo.Transaction.Result.ContractResult, nil
}

func (cc *ChainClient) sendContractRequest(txType common.TxType, mergeSignedPayloadBytes []byte, timeout int64, withSyncResult bool) (*common.TxResponse, error) {
	txId := GetRandTxId()

	resp, err := cc.proposalRequestWithTimeout(txType, txId, mergeSignedPayloadBytes, timeout)
	if err != nil {
		return resp, fmt.Errorf("send %s failed, %s", txType.String(), err.Error())
	}

	if resp.Code == common.TxStatusCode_SUCCESS {
		if !withSyncResult {
			resp.ContractResult = &common.ContractResult{
				Code:    common.ContractResultCode_OK,
				Message: common.ContractResultCode_OK.String(),
				Result:  []byte(txId),
			}
		} else {
			contractResult, err := cc.getSyncResult(txId)
			if err != nil {
				return nil, fmt.Errorf("get sync result failed, %s", err.Error())
			}
			resp.ContractResult = contractResult
		}
	}

	return resp, nil
}

func (cc *ChainClient) sendContractRequestWithTxId(txType common.TxType, mergeSignedPayloadBytes []byte, timeout int64, withSyncResult bool, txId string) (*common.TxResponse, string, error) {
	if txId == "" {
		txId = GetRandTxId()
	}

	resp, err := cc.proposalRequestWithTimeout(txType, txId, mergeSignedPayloadBytes, timeout)
	if err != nil {
		return resp, txId, fmt.Errorf("send %s failed, %s", txType.String(), err.Error())
	}

	if resp.Code == common.TxStatusCode_SUCCESS {
		if !withSyncResult {
			resp.ContractResult = &common.ContractResult{
				Code:    common.ContractResultCode_OK,
				Message: common.ContractResultCode_OK.String(),
				Result:  []byte(txId),
			}
		} else {
			contractResult, err := cc.getSyncResult(txId)
			if err != nil {
				return nil, txId, fmt.Errorf("get sync result failed, %s", err.Error())
			}
			resp.ContractResult = contractResult
		}
	}

	return resp, txId, nil
}
