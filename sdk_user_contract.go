/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_sdk_go

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/gogo/protobuf/proto"

	"chainmaker.org/chainmaker-sdk-go/pb/protogo/accesscontrol"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
)

const (
	// 轮训交易结果最大次数
	retryCnt = 10
)

func (cc *ChainClient) CreateContractCreatePayload(contractName, version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType, kvs []*common.KeyValuePair) ([]byte, error) {
	cc.logger.Debugf("[SDK] create [ContractCreate] to be signed payload")
	return cc.createContractManagePayload(contractName, common.ManageUserContractFunction_INIT_CONTRACT.String(),
		version, byteCodeStringOrFilePath, runtime, kvs)
}

func (cc *ChainClient) CreateContractUpgradePayload(contractName, version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType, kvs []*common.KeyValuePair) ([]byte, error) {
	cc.logger.Debugf("[SDK] create [ContractUpgrade] to be signed payload")
	return cc.createContractManagePayload(contractName, common.ManageUserContractFunction_UPGRADE_CONTRACT.String(),
		version, byteCodeStringOrFilePath, runtime, kvs)
}

func (cc *ChainClient) CreateContractFreezePayload(contractName string) ([]byte, error) {
	cc.logger.Debugf("[SDK] create [ContractFreeze] to be signed payload")
	return cc.createContractOpPayload(contractName, common.ManageUserContractFunction_FREEZE_CONTRACT.String())
}

func (cc *ChainClient) CreateContractUnfreezePayload(contractName string) ([]byte, error) {
	cc.logger.Debugf("[SDK] create [ContractUnfreeze] to be signed payload")
	return cc.createContractOpPayload(contractName, common.ManageUserContractFunction_UNFREEZE_CONTRACT.String())
}

func (cc *ChainClient) CreateContractRevokePayload(contractName string) ([]byte, error) {
	cc.logger.Debugf("[SDK] create [ContractRevoke] to be signed payload")
	return cc.createContractOpPayload(contractName, common.ManageUserContractFunction_REVOKE_CONTRACT.String())
}

func (cc *ChainClient) createContractManagePayload(contractName, method, version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType, kvs []*common.KeyValuePair) ([]byte, error) {
	var (
		err       error
		codeBytes []byte
	)
	cc.logger.Debugf("[SDK] create [ContractManage] to be signed payload")

	isFile := Exists(byteCodeStringOrFilePath)
	if isFile {
		bz, err := ioutil.ReadFile(byteCodeStringOrFilePath)
		if err != nil {
			return nil, fmt.Errorf("read from byteCode file %s failed, %s", byteCodeStringOrFilePath, err)
		}

		if runtime == common.RuntimeType_EVM { // evm contract hex need decode to bytes
			codeBytesStr := strings.TrimSpace(string(bz))
			if codeBytes, err = hex.DecodeString(codeBytesStr); err != nil {
				return nil, fmt.Errorf("decode evm contract hex to bytes failed, %s", err)
			}
		} else { // wasm bin file no need decode
			codeBytes = bz
		}
	} else {
		if runtime == common.RuntimeType_EVM {
			byteCodeStringOrFilePath = strings.TrimSpace(byteCodeStringOrFilePath)
		}

		if codeBytes, err = hex.DecodeString(byteCodeStringOrFilePath); err != nil {
			if codeBytes, err = base64.StdEncoding.DecodeString(byteCodeStringOrFilePath); err != nil {
				return nil, fmt.Errorf("decode byteCode string failed, %s", err)
			}
		}
	}

	payload := &common.ContractMgmtPayload{
		ChainId: cc.chainId,
		ContractId: &common.ContractId{
			ContractName:    contractName,
			ContractVersion: version,
			RuntimeType:     runtime,
		},
		Method:     method,
		Parameters: kvs,
		ByteCode:   codeBytes,
	}

	bytes, err := proto.Marshal(payload)
	if err != nil {
		return nil, fmt.Errorf("construct contract manage payload failed, %s", err)
	}

	return bytes, nil
}

func (cc *ChainClient) createContractOpPayload(contractName, method string) ([]byte, error) {
	payload := &common.ContractMgmtPayload{
		ChainId: cc.chainId,
		ContractId: &common.ContractId{
			ContractName: contractName,
		},
		Method: method,
	}

	bytes, err := proto.Marshal(payload)
	if err != nil {
		return nil, fmt.Errorf("construct contract manage payload failed, %s", err)
	}

	return bytes, nil
}

func (cc *ChainClient) SignContractManagePayload(payloadBytes []byte) ([]byte, error) {
	payload := &common.ContractMgmtPayload{}
	if err := proto.Unmarshal(payloadBytes, payload); err != nil {
		return nil, fmt.Errorf("unmarshal contract manage payload failed, %s", err)
	}

	signBytes, err := signPayload(cc.privateKey, cc.userCrt, payloadBytes)
	if err != nil {
		return nil, fmt.Errorf("SignPayload failed, %s", err)
	}

	sender := &accesscontrol.SerializedMember{
		OrgId:      cc.orgId,
		MemberInfo: cc.userCrtBytes,
		IsFullCert: true,
	}

	entry := &common.EndorsementEntry{
		Signer:    sender,
		Signature: signBytes,
	}

	payload.Endorsement = []*common.EndorsementEntry{
		entry,
	}

	signedPayloadBytes, err := proto.Marshal(payload)
	if err != nil {
		return nil, fmt.Errorf("marshal contract manage sigend payload failed, %s", err)
	}

	return signedPayloadBytes, nil
}

func (cc *ChainClient) MergeContractManageSignedPayload(signedPayloadBytes [][]byte) ([]byte, error) {
	return mergeContractManageSignedPayload(signedPayloadBytes)
}

func (cc *ChainClient) SendContractManageRequest(mergeSignedPayloadBytes []byte, timeout int64, withSyncResult bool) (*common.TxResponse, error) {
	return cc.sendContractRequest(common.TxType_MANAGE_USER_CONTRACT, mergeSignedPayloadBytes, timeout, withSyncResult)
}

func (cc *ChainClient) SendContractManageRequestWithTxId(mergeSignedPayloadBytes []byte, timeout int64, withSyncResult bool, txId string) (*common.TxResponse, string, error) {
	return cc.sendContractRequestWithTxId(common.TxType_MANAGE_USER_CONTRACT, mergeSignedPayloadBytes, timeout, withSyncResult, txId)
}

func (cc *ChainClient) InvokeContract(contractName, method, txId string, params map[string]string, timeout int64, withSyncResult bool) (*common.TxResponse, error) {
	if txId == "" {
		txId = GetRandTxId()
	}

	cc.logger.Debugf("[SDK] begin to INVOKE contract, [contractName:%s]/[method:%s]/[txId:%s]/[params:%+v]",
		contractName, method, txId, params)

	pairs := paramsMap2KVPairs(params)

	payloadBytes, err := constructTransactPayload(contractName, method, pairs)
	if err != nil {
		return nil, fmt.Errorf("construct transact payload failed, %s", err.Error())
	}

	resp, err := cc.proposalRequestWithTimeout(common.TxType_INVOKE_USER_CONTRACT, txId, payloadBytes, timeout)
	if err != nil {
		return resp, fmt.Errorf("%s failed, %s", common.TxType_INVOKE_USER_CONTRACT.String(), err.Error())
	}

	if resp.Code == common.TxStatusCode_SUCCESS {
		if !withSyncResult {
			resp.ContractResult = &common.ContractResult{
				Code:    common.ContractResultCode_OK,
				Message: common.ContractResultCode_OK.String(),
				Result:  []byte(txId),
			}
		} else {
			contractResult, err := cc.getSyncResult(txId)
			if err != nil {
				return nil, fmt.Errorf("get sync result failed, %s", err.Error())
			}

			if contractResult.Code != common.ContractResultCode_OK {
				resp.Code = common.TxStatusCode_CONTRACT_FAIL
				resp.Message = contractResult.Message
			}

			resp.ContractResult = contractResult
		}
	}

	return resp, nil
}

func (cc *ChainClient) QueryContract(contractName, method string, params map[string]string, timeout int64) (*common.TxResponse, error) {
	txId := GetRandTxId()

	cc.logger.Debugf("[SDK] begin to QUERY contract, [contractName:%s]/[method:%s]/[txId:%s]/[params:%+v]",
		contractName, method, txId, params)

	pairs := paramsMap2KVPairs(params)

	payloadBytes, err := constructQueryPayload(contractName, method, pairs)
	if err != nil {
		return nil, fmt.Errorf("construct query payload failed, %s", err.Error())
	}

	resp, err := cc.proposalRequestWithTimeout(common.TxType_QUERY_USER_CONTRACT, txId, payloadBytes, timeout)
	if err != nil {
		return nil, fmt.Errorf("send %s failed, %s", common.TxType_QUERY_USER_CONTRACT.String(), err.Error())
	}

	return resp, nil
}

func (cc *ChainClient) GetTxRequest(contractName, method, txId string, params map[string]string) (*common.TxRequest, error) {
	if txId == "" {
		txId = GetRandTxId()
	}

	cc.logger.Debugf("[SDK] begin to create TxRequest, [contractName:%s]/[method:%s]/[txId:%s]/[params:%+v]",
		contractName, method, txId, params)

	pairs := paramsMap2KVPairs(params)

	payloadBytes, err := constructTransactPayload(contractName, method, pairs)
	if err != nil {
		return nil, fmt.Errorf("construct transact payload failed, %s", err.Error())
	}

	req, err := cc.generateTxRequest(txId, common.TxType_INVOKE_USER_CONTRACT, payloadBytes)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func (cc *ChainClient) SendTxRequest(txRequest *common.TxRequest, timeout int64, withSyncResult bool) (*common.TxResponse, error) {

	resp, err := cc.sendTxRequest(txRequest, timeout)
	if err != nil {
		return resp, fmt.Errorf("%s failed, %s", common.TxType_INVOKE_USER_CONTRACT.String(), err.Error())
	}

	if resp.Code == common.TxStatusCode_SUCCESS {
		if !withSyncResult {
			resp.ContractResult = &common.ContractResult{
				Code:    common.ContractResultCode_OK,
				Message: common.ContractResultCode_OK.String(),
				Result:  []byte(txRequest.Header.TxId),
			}
		} else {
			contractResult, err := cc.getSyncResult(txRequest.Header.TxId)
			if err != nil {
				return nil, fmt.Errorf("get sync result failed, %s", err.Error())
			}

			if contractResult.Code != common.ContractResultCode_OK {
				resp.Code = common.TxStatusCode_CONTRACT_FAIL
				resp.Message = contractResult.Message
			}

			resp.ContractResult = contractResult
		}
	}

	return resp, nil
}
